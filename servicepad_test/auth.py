from flask import Blueprint, request
import models
from werkzeug.security import generate_password_hash, check_password_hash
from app import db
from flask_login import login_user, login_required, logout_user
import base64

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=['GET', 'POST'])
def login():
    message = ''
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')
        user = models.Users.query.filter_by(email=email).first()
        if check_password_hash(user.password, password):
            message = 'You have logged in!'
            login_user(user, remember=True)
            return message
        else:
            message = 'Incorrect email or password.'
    return message


@auth.route('/logout', methods=['GET'])
@login_required
def logout():
    logout_user()
    return 'You\'ve logged out!'


@auth.route('/sign-up', methods=['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        email = request.form.get('email')
        fullname = request.form.get('fullname')
        password = request.form.get('password')
        photo = base64.b64encode(request.files['photo'].read())
        user = models.Users.query.filter_by(email=email).first()
        print(user)
        if user:
            message = 'Email already exists.'
        else:
            new_user = models.Users(email=email, photo=photo, fullname=fullname,
                                   password=generate_password_hash(password, method='sha256'))
            print(new_user)
            db.session.add(new_user)
            db.session.commit()
            # login_user(new_user, remember=True)
            message = 'You\'ve successfully signed up!'
            return message

    return message
