from flask import Flask, request, jsonify, redirect, url_for, render_template
from flask_sqlalchemy import SQLAlchemy
# from flask_login import UserMixin, login_required
from flask_login import LoginManager


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://sptest:sptest@localhost/sptest'
app.config['SECRET_KEY'] = 'secret-key'
app.config['SECURITY_REGISTERABLE'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

app.debug = True
db = SQLAlchemy(app)


import models
db.create_all()
db.session.commit()

# Setup Flask-Security
# user_datastore = SQLAlchemyUserDatastore(db, User, Role)
# security = Security(app, user_datastore)


@app.route('/')
def index():
    return render_template('index.html')


from views import views as route_views
app.register_blueprint(route_views)
from auth import auth as route_auth
app.register_blueprint(route_auth)

login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.init_app(app)


@login_manager.user_loader
def load_user(id):
    return models.Users.query.get(int(id))


if __name__ == "__main__":
    app.run()
