from app import db
from datetime import datetime
from flask_login import UserMixin


class Priority(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(30), nullable=False)
    publications = db.relationship('Publication', backref='priority', lazy='dynamic')

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return '<Priority %r>' % self.id


class Status(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(30), nullable=False)
    publications = db.relationship('Publication', backref='status', lazy='dynamic')

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return '<Status %r>' % self.id


class Users(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    fullname = db.Column(db.String(255))
    photo = db.Column(db.LargeBinary)
    publications = db.relationship('Publication', backref='user', lazy='dynamic')

    def __init__(self, email, password, fullname, photo):
        self.email = email
        self.password = password
        self.fullname = fullname
        self.photo = photo

    def __repr__(self):
        return '<Publication %r>' % self.id


class Publication(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(30), nullable=False)
    description = db.Column(db.String(200), nullable=False)
    priority_id = db.Column(db.Integer, db.ForeignKey(Priority.__table__.c.id))
    status_id = db.Column(db.Integer, db.ForeignKey(Status.__table__.c.id))
    time_since = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey(Users.__table__.c.id))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __init__(self, title, description, priority_id, status_id, time_since, user_id, created_at, updated_at):
        self.title = title
        self.description = description
        self.priority_id = priority_id
        self.status_id = status_id
        self.time_since = time_since
        self.user_id = user_id
        self.created_at = created_at
        self.updated_at = updated_at

    def __repr__(self):
        return '<Publication %r>' % self.id
