from flask import Blueprint, request, jsonify
import models
from app import db


views = Blueprint('views', __name__)


# Priority CRUDs

@views.route('/priority/<id>', methods=['GET'])
def get_priority(id):
  priority = models.Priority.query.get(id)
  del priority.__dict__['_sa_instance_state']
  return jsonify(priority.__dict__)


@views.route('/priority', methods=['GET'])
def get_priorities():
  priority = []
  for p in db.session.query(models.Priority).all():
    del p.__dict__['_sa_instance_state']
    priority.append(p.__dict__)
  return jsonify(priority)


@views.route('/priority', methods=['POST'])
def create_priority():
  body = request.get_json()
  db.session.add(models.Priority(body['title']))
  db.session.commit()
  return "Priority created"


@views.route('/priority/<id>', methods=['PUT'])
def update_priority(id):
  body = request.get_json()
  db.session.query(models.Priority).filter_by(id=id).update(
    dict(title=body['title']))
  db.session.commit()
  return "Priority updated"


@views.route('/priority/<id>', methods=['DELETE'])
def delete_priority(id):
  db.session.query(models.Priority).filter_by(id=id).delete()
  db.session.commit()
  return "Priority deleted"


# Status CRUDs

@views.route('/status/<id>', methods=['GET'])
def get_status(id):
  status = models.Status.query.get(id)
  del status.__dict__['_sa_instance_state']
  return jsonify(status.__dict__)


@views.route('/status', methods=['GET'])
def get_statuses():
  status = []
  for s in db.session.query(models.Status).all():
    del s.__dict__['_sa_instance_state']
    status.append(s.__dict__)
  return jsonify(status)


@views.route('/status', methods=['POST'])
def create_status():
  body = request.get_json()
  db.session.add(models.Status(body['title']))
  db.session.commit()
  return "Status created"


@views.route('/status/<id>', methods=['PUT'])
def update_status(id):
  body = request.get_json()
  db.session.query(models.Status).filter_by(id=id).update(
    dict(title=body['title']))
  db.session.commit()
  return "Status updated"


@views.route('/status/<id>', methods=['DELETE'])
def delete_status(id):
  db.session.query(models.Status).filter_by(id=id).delete()
  db.session.commit()
  return "Status deleted"


# User CRUDs

@views.route('/user/<id>', methods=['GET'])
def get_user(id):
  user = models.Users.query.get(id)
  del user.__dict__['_sa_instance_state']
  return jsonify(user.__dict__)


@views.route('/user', methods=['GET'])
def get_users():
  user = []
  for u in db.session.query(models.Users).all():
    del u.__dict__['_sa_instance_state']
    user.append(u.__dict__)
  return jsonify(user)


@views.route('/user', methods=['POST'])
def create_user():
  body = request.get_json()
  db.session.add(models.Users(body['email'], body['password'], body['fullname'], body['photo']))
  db.session.commit()
  return "User created"


@views.route('/user/<id>', methods=['PUT'])
def update_user(id):
  body = request.get_json()
  db.session.query(models.Users).filter_by(id=id).update(
    dict(email=body['email'], password=body['password'], fullname=body['fullname'], photo=body['photo']))
  db.session.commit()
  return "User updated"


@views.route('/user/<id>', methods=['DELETE'])
def delete_user(id):
  db.session.query(models.Users).filter_by(id=id).delete()
  db.session.commit()
  return "User deleted"


# Publication CRUDs

@views.route('/publication/<id>', methods=['GET'])
def get_publication(id):
  publication = models.Publication.query.get(id)
  del publication.__dict__['_sa_instance_state']
  return jsonify(publication.__dict__)


@views.route('/publication', methods=['GET'])
def get_publications():
  publication = []
  for p in db.session.query(models.Publication).all():
    del p.__dict__['_sa_instance_state']
    publication.append(p.__dict__)
  return jsonify(publication)


@views.route('/publication', methods=['POST'])
def create_publication():
  body = request.get_json()
  db.session.add(models.Publication(body['title'], body['description'], body['priority_id'], body['status_id'],
                                    body['time_since'], body['user_id'], body['created_at'], body['update_at']))
  db.session.commit()
  return "Publication created"


@views.route('/publication/<id>', methods=['PUT'])
def update_publication(id):
  body = request.get_json()
  db.session.query(models.Publication).filter_by(id=id).update(
    dict(title=body['title'], description=body['description'], priority_id=body['priority_id'],
         status_id=body['status_id'], time_since=body['time_since'], user_id=body['user_id'],
         created_at=body['created_at'], update_at=body['update_at']))
  db.session.commit()
  return "Publication updated"


@views.route('/publication/<id>', methods=['DELETE'])
def delete_publication(id):
  db.session.query(models.Publication).filter_by(id=id).delete()
  db.session.commit()
  return "Publication deleted"
